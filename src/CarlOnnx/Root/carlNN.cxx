// CARLNN includes
#include "CarlOnnx/carlNN.h"

// OS includes
#include "PathResolver/PathResolver.h"
#include <sstream>
#include <map>
#include <math.h>
#include <cassert>
#include <unordered_map>

// Athena includes
#include <AsgTools/MessageCheck.h>

using namespace asg::msgUserCode;

//Class Constructor
CARLNN::CARLNN( std::string name,
		std::string ONNXModelPath, 
		std::unordered_map<std::string, float> *inputTensor,
		bool doDebug ){

  //Set basic debug option from user
  //Default = false as seen in CARLNN.h
  m_DoDebug = doDebug;


  //Set the name of the tool
  m_name = name;

  //Initialise as base call in class constructor the ONNXruntime API Base
  m_g_ort = OrtGetApiBase()->GetApi(ORT_API_VERSION);

  //Link via C++ pointer address input dataset
  //InputDataLink( *inputTensor );
  m_inputTensor = inputTensor;
  
  //Call intiaitlisation for NN model architecture
  initialise( ONNXModelPath );

  //Validate that meta data of onnx neural network model 
  //with that of the input data
  //CARLNN::ValidateMetaAndInputData();
}

CARLNN::~CARLNN()
{
}

// Initialisation function:
//   *Brief: User provides necessary ONNXruntime model path location
void CARLNN::initialise( std::string ONNXModelPath ){
  
  // Initialize  enviroment...one enviroment per process
  // enviroment maintains thread pools and other state info
  CheckStatus(m_g_ort->CreateEnv(ORT_LOGGING_LEVEL_WARNING, m_name.c_str(), &m_env));
  
  // Initialize session options if needed
  CheckStatus(m_g_ort->CreateSessionOptions(&m_session_options));
  CheckStatus(m_g_ort->SetIntraOpNumThreads(m_session_options, 1));
  
  // Sets graph optimization level
  CheckStatus(m_g_ort->SetSessionGraphOptimizationLevel(m_session_options, ORT_ENABLE_BASIC));
  std::string model_path = PathResolver::find_file (ONNXModelPath, "DATAPATH");
  
  //Create ONNXruntime Session
  if(m_DoDebug) { ANA_MSG_INFO("Using Onnxruntime C API\n"); }
  CheckStatus(m_g_ort->CreateSession(m_env, model_path.c_str(), m_session_options, &m_session));
  CheckStatus(m_g_ort->GetAllocatorWithDefaultOptions(&m_allocator));
  
  //Obtain the abstract input layer size
  //    Example:   Some NN architectures have a deep set NN or a RNN paired with
  //               a typical dense NN as an input. This is interepreted
  //               as an input tensor of rank 2 (RNN + DNN), but of course
  //               each input can have n-degrees of freedom output
  //               Therefore you might have n+n' inputs, but with an 
  //               abstract layer of 2 dof from this call.
  m_status = m_g_ort->SessionGetInputCount(m_session, &m_num_input_nodes);
  //Obtain model metadata 
  CheckStatus(m_g_ort->SessionGetModelMetadata(m_session, &m_meta_data));
  if(m_DoDebug) std::cout << "m_meta_data"<< &(m_meta_data) << std::endl;
  CheckStatus(m_g_ort->ModelMetadataGetCustomMetadataMapKeys(m_meta_data, m_allocator, &keys,  &num_keys));
  if(m_DoDebug) std::cout << "num_keys "<< num_keys << std::endl;
  //CheckStatus(m_g_ort->ModelMetadataLookupCustomMetadataMap(m_meta_data, m_allocator, "Var1", &value));
  //std::cout << "var "<< value << std::endl;
  for (long int  i = 0; i < num_keys; i++)
  {
      if(m_DoDebug) std::cout << "keys "<< keys[i] << std::endl;
      //CheckStatus(m_g_ort->ModelMetadataLookupCustomMetadataMap(m_meta_data, m_allocator, keys[i], &value));
      CheckStatus(m_g_ort->ModelMetadataLookupCustomMetadataMap(m_meta_data, 
								m_allocator, 
								keys[i], 
								&value));
      // Convert to std::string
      std::string PyListStr(value);
      PyListStr.erase(0,1); //Remove first '{'
      PyListStr.erase(PyListStr.size()-1); //Remove last '}'
      // Tokenise string based on delimiters
      std::istringstream iPyListStr(PyListStr); 
      std::string part;
      std::vector<float> tmpvalVec;
      while (getline(iPyListStr,part, ',')){
	  if(m_DoDebug) std::cout << "part: "<< part  << std::endl;
	  tmpvalVec.push_back(stof(part));
	  if(m_DoDebug) std::cout << "part(float): "<< tmpvalVec.back()  << std::endl;
      }
      
      //for( int j = 0; j < ((sizeof value)/(sizeof value[0])); j++){
      //std::cout << "value "<< value[j] << std::endl;
      ////float val = atof(value[j]);
      //float val = strtof(&(value[j]), NULL);
      //tmpvalVec.push_back(val);
      //}
      //keyVec.push_back(keys[i]); 
      //valVec.push_back(value[i]); 
      m_metadata[keys[i]] = tmpvalVec;
  }
  //std::cout << "keyVec "<< keyVec[0] << std::endl;

  //m_input_node_names.push_back((const char*)m_num_input_nodes);
  m_input_node_dims.push_back(m_num_input_nodes);
  if(m_DoDebug) {ANA_MSG_INFO("Number of inputs = "<< m_num_input_nodes); }

  // Obtain the abstract output layer number
  //     Example:   As in the case of the above input call, the output
  //                can have a tensor rank that is 2 because the output is
  //                2 sets of vectors (multi-classifier) with regression outputs
  //                for example
  m_status_Out = m_g_ort->SessionGetOutputCount(m_session, &m_num_output_nodes);
  //m_output_node_names.push_back((const char*)m_num_output_nodes);
  m_output_node_dims.push_back(m_num_output_nodes);
  if(m_DoDebug) { ANA_MSG_INFO("Number of outputs = "<< m_num_output_nodes); }


  //Step 1:  Run over input node and store metadata
  for (size_t i = 0; i < m_num_input_nodes; i++) {
      
    char* input_name;
    m_status = m_g_ort->SessionGetInputName(m_session, i, m_allocator, &input_name);
    //m_input_node_names[i] = input_name;
    m_input_node_names.push_back(input_name);
    // print input node types
    if(m_DoDebug) { ANA_MSG_INFO("Input  : "<< i <<" name : "<< input_name); }
      
    OrtTypeInfo* typeinfo;
    m_status = m_g_ort->SessionGetInputTypeInfo(m_session, i, &typeinfo);
    const OrtTensorTypeAndShapeInfo* tensor_info;
    CheckStatus(m_g_ort->CastTypeInfoToTensorInfo(typeinfo, &tensor_info));
    ONNXTensorElementDataType type;
    CheckStatus(m_g_ort->GetTensorElementType(tensor_info, &type));
    // print input shapes/dims
    if(m_DoDebug) { ANA_MSG_INFO("Input  : "<< i <<" type : "<< type); }
      
    //Call to calculate the number of dimensions as direct input to the NN
    size_t num_dims;
    CheckStatus(m_g_ort->GetDimensionsCount(tensor_info, &num_dims));
    if(m_DoDebug) { ANA_MSG_INFO("Input  : "<< i <<  " dims : "<< num_dims); }
    m_input_node_dims.resize(num_dims);
    CheckStatus(m_g_ort->GetDimensions(tensor_info, (int64_t*)m_input_node_dims.data(), num_dims));
    for (size_t j = 0; j < num_dims; j++){
      if(m_DoDebug) { ANA_MSG_INFO("Input  : "<< i <<  " j : "<< j << " dims: "<< m_input_node_dims[j]); }
    }
    m_g_ort->ReleaseTypeInfo(typeinfo);
  }

  // Step 2:   Run over and store output node metadata
  // iterate over all output nodes
  for (size_t i = 0; i < m_num_output_nodes; i++) {
    // print output node names
    char* output_name;
    m_status_Out = m_g_ort->SessionGetOutputName(m_session, i, m_allocator, &output_name);
    // print output node types
    if(m_DoDebug) { ANA_MSG_INFO("Output : "<< i <<  " name : "<< output_name); }
    //m_output_node_names[i] = output_name;
    m_output_node_names.push_back(output_name);
          
    
    OrtTypeInfo* typeinfo;
    m_status_Out = m_g_ort->SessionGetOutputTypeInfo(m_session, i, &typeinfo);
    const OrtTensorTypeAndShapeInfo* tensor_info;
    CheckStatus(m_g_ort->CastTypeInfoToTensorInfo(typeinfo, &tensor_info));
    ONNXTensorElementDataType type;
    CheckStatus(m_g_ort->GetTensorElementType(tensor_info, &type));
    // print output shapes/dims
    if(m_DoDebug) { ANA_MSG_INFO("Output : "<< i <<" type : "<< type); }
    
    //Call to calculate the number of output dimensions
    size_t num_dims;
    CheckStatus(m_g_ort->GetDimensionsCount(tensor_info, &num_dims));
    if(m_DoDebug) { ANA_MSG_INFO("Output : "<<i <<" num_dims : "<< num_dims); }
    m_output_node_dims.resize(num_dims);
    CheckStatus(m_g_ort->GetDimensions(tensor_info, (int64_t*)m_output_node_dims.data(), num_dims));
    for (size_t j = 0; j < num_dims; j++){
      if(m_output_node_dims[j]<0){ m_output_node_dims[j] =1;}
      if(m_DoDebug) { ANA_MSG_INFO("Output  : "<< i <<  " j : "<< j << " dims: "<< m_output_node_dims[j]); }
    }
    m_g_ort->ReleaseTypeInfo(typeinfo);
  }


  // Define input data vector for compatible onnx evaluation call in CARLNN::evaluate()
  //m_input_tensor_size = m_inputTensor->size(); //User would need to specify first might not be the case so avoid for now
  m_input_tensor_size = num_keys; //Meta data size should match input. As we impose this, this is ok at this stage
  m_input_tensor_values.reserve(m_input_tensor_size); //Reserve for efficiency but might be less flexible

  //return StatusCode::SUCCESS;
}

// Data linking function:
//   *Brief: Link input data to onnx model from the user interface to CARLNN data member
//   *Note: Potential for templating to allow for multiple data structure types
void CARLNN::InputDataLink( std::unordered_map<std::string, float>* inputTensor){

  if(m_DoDebug) { 
    ANA_MSG_INFO("CARLNN::InputDataLink()"); 
    //for(auto &item: inputTensor){
    //  std::cout << "<ValidateMetaAndInputData()>::   Input tensor: " << item.first << std::endl;
    //}
  }

  //Equate CARLNN data member to user input tensor
  m_inputTensor = inputTensor;

  //Check map is same as input pointer
  //if(m_DoDebug) { 
  //  for(auto &item: *m_inputTensor){
  //    std::cout << "<ValidateMetaAndInputData()>::   Input tensor (member): " << item.first << std::endl;
  //  }
  //}
}

// Input data pre-processing function:
//   *Brief: Pre-process input tenor using CARLNN::inputTensor and CARLNN::InputMetaData
void CARLNN::PreProcessInputData(){

  if(m_DoDebug) { ANA_MSG_INFO("CARLNN::PreProcessInputData()"); }
  
  //Simple approach for now - assignment callback is best way to go
  for( auto &var: (*m_inputTensor)){
    //for( auto &element: m_metadata){

      float min, max;
      
      if(m_metadata[var.first].front() < m_metadata[var.first].back())
      {
	  min = m_metadata[var.first].front();
	  max = m_metadata[var.first].back();
      }
      else
      {
	  min = m_metadata[var.first].back();
	  max = m_metadata[var.first].front();
      }

      // Scale minmax
      //std::cout << "" << var.first << "    diff = " <<  fabs(max-min) << ",    numeric_limits:   " << std::numeric_limits<float>::epsilon() << std::endl;
      float diff = fabs(max-min) > 10 * std::numeric_limits<float>::epsilon() ? (max-min) : 1.0;
      var.second =  (var.second - min)/diff;
      
    // var.second =  (var.second - m_metadata[var.first].front())/m_metadata[var.first].back();
      

    //if(m_DoDebug) { ANA_MSG_INFO("CARLNN::PreProcessInputData() " << var.first << ": (x-"<< min <<")/"<< max); }
      if(m_DoDebug) { ANA_MSG_INFO("CARLNN::PreProcessInputData() " << var.first << ": (x-"<< min <<")/"<< diff); }
    //float *value = m_inputTensor[element.first];
    //(*value) =  ((*value) - m_metadata[element.first].front())/m_metadata[element.first].back();
  }  
}

// Match metadata from onnx model store and input tensor data
//   *Brief: Ensure match between input tensor data and onnx model metadata
void CARLNN::ValidateMetaAndInputData(){

  if(m_DoDebug) { ANA_MSG_INFO("CARLNN::ValidateMetaAndInputData()"); }

  //Loop through metadata from onnx
  for(auto &item: m_metadata)
  {  
      if(m_DoDebug) std::cout << "<ValidateMetaAndInputData()>::   metadata: " << item.first << std::endl;
      //Find in input tensor data map same variable
      if( m_inputTensor->find(item.first) == m_inputTensor->end() )
      {
	  std::cout << "<ValidateMetaAndInputData()>::   Input tensor does not contain variable '" << item.first << "', therefore exiting program." << std::endl;
	  exit(1); //Exit with code result 1
      }   
  }
}

// Evaluate function:
//   *Brief: Evaluate ONNX NN CARL model using input space x=[x1,x2,...xn]
float CARLNN::evaluate(){

  // Debug call for function and user diagnostic assistance
  if(m_DoDebug) { ANA_MSG_INFO("CARLNN::evaluate()"); }
  
  // Step 3a:   Calculate NN output values from inputs
  //            -> preparing std::vector container to hold input data
  if(m_DoDebug) { ANA_MSG_INFO("CARLNN::evaluate()   input_tensor_values size: " << m_input_tensor_size); }
  //for( auto &item: (*m_inputTensor)){
  PreProcessInputData();
  // Retains capacity simply clears all elements/clears pointers/references etc...
  m_input_tensor_values.clear();
  for( auto &item: m_metadata)
  {
      if(m_DoDebug){
	  std::cout << "CARLNN::evaluate()   Var: " << item.first << std::endl;
	  std::cout << "CARLNN::evaluate()     Value: " << m_inputTensor->find(item.first)->second  << std::endl;
      }
      
      m_input_tensor_values.push_back(m_inputTensor->find(item.first)->second);

      if(m_DoDebug){
	  std::cout << "CARLNN::evaluate()     Value(vec): " << m_input_tensor_values.back() << std::endl;
      }
  }
      
  // create input tensor object from data values
  OrtMemoryInfo* memory_info;
  CheckStatus(m_g_ort->CreateCpuMemoryInfo(OrtArenaAllocator, OrtMemTypeDefault, &memory_info));
  OrtValue* input_tensor = NULL;
  
  if(m_DoDebug) { ANA_MSG_INFO("CARLNN::evaluate()   Create Tensor of ORT data type"); }
  CheckStatus( m_g_ort->CreateTensorWithDataAsOrtValue(memory_info, 
       m_input_tensor_values.data(), 
       m_input_tensor_size * sizeof(float) , 
       m_input_node_dims.data(),  
       m_input_node_dims.size(), 
       ONNX_TENSOR_ELEMENT_DATA_TYPE_FLOAT, 
       &input_tensor));
  int is_tensor;
  CheckStatus(m_g_ort->IsTensor(input_tensor, &is_tensor));
  assert(is_tensor);
  m_g_ort->ReleaseMemoryInfo(memory_info);
  
  // score model & input tensor, get back output tensor
  static const char* output_names[] = { "r_hat", "s_hat"};
  static const size_t num_outputs = sizeof( output_names ) / sizeof(output_names[ 0 ] );
  OrtValue* output_tensor[2];
  output_tensor[0] = NULL;
  output_tensor[1] = NULL;
  
  if(m_DoDebug) {
    ANA_MSG_INFO("CARLNN::evaluate()   Run evaluation of ONNX model");
    ANA_MSG_INFO("CARLNN::evaluate()       m_input_node_names size: " << m_input_node_names.size());
    ANA_MSG_INFO("CARLNN::evaluate()       num_outputs: " << num_outputs);
    ANA_MSG_INFO("CARLNN::evaluate()   m_g_ort->Run()");
  }
  CheckStatus( m_g_ort->Run(m_session, 
			    NULL, 
			    m_input_node_names.data(), 
			    &input_tensor, 
			    m_input_node_names.size(), 
			    output_names, num_outputs, 
			    (OrtValue**)&output_tensor));
  CheckStatus(m_g_ort->IsTensor(*output_tensor, &is_tensor));
  assert(is_tensor);
  
  // Get pointer to output tensor float values
  float* r_hat;
  float* s_hat;
  CheckStatus(m_g_ort->GetTensorMutableData(output_tensor[0], (void**)&r_hat));
  CheckStatus(m_g_ort->GetTensorMutableData(output_tensor[1], (void**)&s_hat));
  std::string s = typeid(s_hat).name();
  std::cout << "s: " << s << std::endl;

  // show  true label for the test input
  float max = -999;
  int max_index{0};
  for (size_t i = 0; i < m_num_output_nodes; i++){
    std::cout << "s_hat[" << i << "] = " << s_hat[i] << std::endl; 
    std::cout << "r_hat[" << i << "] = " << r_hat[i] << std::endl; 
    if (max<s_hat[i]){
      max = s_hat[i];
      max_index = i;
    }
  }
  float weight = 1./r_hat[max_index];
  if(m_DoDebug) {
    ANA_MSG_INFO("r_hat for class "<<max_index<<" has the highest score: "<<r_hat[max_index]);
    ANA_MSG_INFO("s_hat for class "<<max_index<<" has the highest score: "<<s_hat[max_index]);
    ANA_MSG_INFO("s_hat from r_hat  "<<(1-s_hat[max_index])/s_hat[max_index]);
    ANA_MSG_INFO("Weight w is r^{-1}: "<<weight);
  }
  //Release values from owneship of ONNXruntume
  m_g_ort->ReleaseValue(*output_tensor);
  m_g_ort->ReleaseValue(input_tensor);
 
  return weight;

}


// Finalise function: 
//    *Brief:  Finalise CARLNN class by deleting memory and cleaning up
//             cached data
EL::StatusCode CARLNN::finalise(){

  // Delete the environment object.
  m_g_ort->ReleaseSession(m_session);
  m_g_ort->ReleaseSessionOptions(m_session_options);
  m_g_ort->ReleaseEnv(m_env);
  if(m_DoDebug){ ANA_MSG_DEBUG( "Ort::Env object deleted" );}

  return EL::StatusCode::SUCCESS;
}


void CARLNN::CheckStatus(OrtStatus* status)
{
  if (status != nullptr) {
    const char* msg = m_g_ort->GetErrorMessage(status);
    fprintf(stderr, "%s\n", msg);
    m_g_ort->ReleaseStatus(status);
    exit(1);
  }
}
