# The name of the package
ATLAS_SUBDIR(CarlOnnx)

# Add binary
ATLAS_ADD_LIBRARY ( CarlOnnxLib CarlOnnx/carlNN.h Root/carlNN.cxx
		      PUBLIC_HEADERS CarlOnnx
		      INCLUDE_DIRS ${ONNXRUNTIME_INCLUDE_DIRS}
		      LINK_LIBRARIES ${ONNXRUNTIME_LIBRARIES}
		      		     PathResolver
				     AsgTools
				     EventLoop)