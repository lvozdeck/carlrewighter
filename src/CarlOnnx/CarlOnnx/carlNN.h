#ifndef CARLNN_H
#define CARLNN_H 1

// Basic C++ STD library includes
#include <iostream>
#include <map>
#include <cassert>
#include <unordered_map>

// OnnxRuntime include(s).
#include <core/session/onnxruntime_cxx_api.h>

// Athena includes
//#include "AthAnalysisBaseComps/AthAnalysisAlgorithm.h"
#include <AsgTools/MessageCheck.h>
#include <EventLoop/StatusCode.h>

// Class:  CARLNN
//   *Brief:  Neural Network architecture interface class for 
//            ATHENA framework. Utilises models stored via ONNXruntime.
//class CARLNN: public ::AthAnalysisAlgorithm{
class CARLNN{

 public:
  //Class Constructor
  CARLNN( std::string m_name,
	  std::string ONNXModelPath,
	  std::unordered_map<std::string, float> *inputTensor,
	  bool doDebug = false );
  //Class Destructor
  ~CARLNN();
  // Initialisation function:
  //   *Brief: User provides necessary ONNXruntime model path location
  void initialise( std::string ONNXModelPath );
  // Evaluate function:
  //   *Brief: Evaluate ONNX NN CARL model using input space x=[x1,x2,...xn]
  float evaluate( );
  // Finalise function: 
  //    *Brief:  Finalise CARLNN class by deleting memory and cleaning up
  //             cached data
  EL::StatusCode finalise();
  // Check status of ONXruntTime function
  //    *Brief:  Checks the status of the ORT objects following operations
  //             performed during initialisation/evaluation/finalisation
  void CheckStatus(OrtStatus* status);
  // Data linking function:
  //   *Brief: Link input data to onnx model from the user interface to CARLNN data member
  void InputDataLink( std::unordered_map<std::string, float>* inputTensor);
  // Input data pre-processing function:
  //   *Brief: Pre-process input tenor using CARLNN::inputTensor and CARLNN::InputMetaData
  void PreProcessInputData();
  // Match metadata from onnx model store and input tensor data
  //   *Brief: Ensure match between input tensor data and onnx model metadata
  void ValidateMetaAndInputData();

  

 private:
  //Class generic member variables
  bool m_DoDebug = false;
  std::string m_name;
  
  /// Onnx runtime environment interfaces
  const OrtApi* m_g_ort;
  OrtEnv* m_env;
  OrtSession* m_session;
  OrtSessionOptions* m_session_options;
  OrtModelMetadata* m_meta_data;  
  OrtAllocator* m_allocator; 

  //Onnx status
  OrtStatus* m_status;     
  OrtStatus* m_status_Out;   

  //Neural Network data structure elements
  size_t m_num_input_nodes;   
  size_t m_num_output_nodes; 
  std::vector<int64_t> m_input_node_dims;
  std::vector<int64_t> m_output_node_dims;
  std::vector<const char*> m_input_node_names;
  std::vector<const char*> m_output_node_names;
  
  // Neural Network meta-data for 
  std::vector<char*> keyVec;
  std::vector<char*> valVec;
  char** keys;
  char* value;
  int64_t num_keys;
  std::map<std::string, std::vector<float> > m_metadata;

  // Input Tensor for Neural Network training
  //std::map<std::string, float> *m_inputTensor; //Pre-processing
  std::unordered_map<std::string, float> *m_inputTensor; //Pre-processing
  std::vector<float> m_input_tensor_values;    //onnx compatible
  size_t m_input_tensor_size; //May duplicate 'm_num_input_nodes' -> CHECK

};

#endif //> !CARLNN_H
