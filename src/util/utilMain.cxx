#include <iostream>
#include <string>
#include <unordered_map>

#include "CarlOnnx/carlNN.h"

#include "ReturnCheck.h"

int main(int argc, char **argv)
{
    if(argc<2)
    {
	std::cout << "No argument provided. Exiting..." << std::endl;
	std::cout << argv[0] << " [path to ONNX file]" << std::endl;
	return 0;
    }
    
    std::string pathToONNXfile = argv[1];
    std::cout << "Input NN: " << pathToONNXfile << std::endl;

    float weight;

    //initialize the input tensor
    std::unordered_map<std::string, float> inputTensor;

    //initialize the NN; give it a pointer to inputTensor

    CARLNN myCarl{"Test", pathToONNXfile,&inputTensor, true};


    inputTensor["MET"] = 6.901829;
    inputTensor["Mtop"] = 143.427185;
    inputTensor["dPhiLBmin"] = 1.852949;
    inputTensor["dPhiVBB"] = 3.084115;
    inputTensor["dRBB"] = 0.614932;
    inputTensor["dYWH"] = 0.621882;
    inputTensor["mBB"] = 70.177620;
    inputTensor["mTW"] = 40.955818;
    inputTensor["nJ"] = 3.0;
    inputTensor["nTags"] = 2.0;
    inputTensor["pTB1"] = 265.392456;
    inputTensor["pTB2"] = 33.026028;
    inputTensor["pTV"] = 100.427750;
    inputTensor["FlavourLabel"] = 0;
    
    weight = myCarl.evaluate();
    std::cout << "Weight: " << weight << std::endl;

    // ##### Real Event 1 #####
    //inputTensor["MET"] = 26.901829;
    //inputTensor["dPhiLBmin"] = 1.852949;
    //inputTensor["dPhiVBB"] = 3.084115;
    //inputTensor["dRBB"] = 1.614932;
    //inputTensor["dYWH"] = 0.621882;
    //inputTensor["mBB"] = 70.177620;
    //inputTensor["mTW"] = 40.955818;
    //inputTensor["Mtop"] = 143.427185;
    //inputTensor["pTB1"] = 65.392456;
    //inputTensor["pTB2"] = 33.026028;
    //inputTensor["pTV"] = 100.427750;
    //inputTensor["nTags"] = 2.0;
    //inputTensor["nJ"] = 3.0;    
    //
    //weight = myCarl.evaluate();
    //std::cout << "Weight: " << weight << std::endl;

    /*// ##### Real Event 2 #####
    inputTensor["MET"] = 30.862650;
    inputTensor["dPhiLBmin"] = 0.616292;
    inputTensor["dPhiVBB"] = 2.805501;
    inputTensor["dRBB"] = 3.522043;
    inputTensor["dYWH"] = 0.546619;
    inputTensor["mBB"] = 185.447876;
    inputTensor["mTW"] = 57.459553;
    inputTensor["Mtop"] = 108.841690;
    inputTensor["pTB1"] = 87.846802;
    inputTensor["pTB2"] = 50.320827;
    inputTensor["pTV"] = 75.178986;
    inputTensor["nTags"] = 2.0;
    inputTensor["nJ"] = 2.0;

    // evaluate and print the weight
    weight = myCarl.evaluate();
    std::cout << "Weight: " << weight << std::endl;
    */

    EL_CHECK("myCarl.finalise()",
	     myCarl.finalise());
    
    return 0;
}
