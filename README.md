## Compile

```bash
mkdir build
cd build
setupATLAS
asetup AnalysisBase,21.2.209
cmake ../src
cmake --build .
```

## Run

```bash
source x*/setup.sh
utilExecutable [path to ONNX file]
```
