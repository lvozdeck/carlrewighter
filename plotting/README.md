# Making Closure Plots
The histograms output by the CxAOD Reader must be hadded into a single root file.

```bash
lsetup root
python plot.py path/to/histograms/hadded_file.root
```