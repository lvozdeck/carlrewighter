from numpy import var
import scipy.special
import ROOT
import array
import sys
import math

def getLinSpace(start,end,N):
    return [start + x/(float(N))*(end-start) for x in range(0,N+1,1)]

binning = {
    "mBB" : getLinSpace(50,300,25), #list(range(50,310,10)),
    "pTV" : getLinSpace(75,400,65), #list(range(75,410,10)),
    "mva" : getLinSpace(-1.0,1.0,10),
    "dPhiVBB" : getLinSpace(0.0,math.pi,50),
    "dRBB" : getLinSpace(0.4,3.1,27),
    "Mtop" :  getLinSpace(90,500,41),
    "mTW" : getLinSpace(0,250,25),
    "pTB1" : getLinSpace(0,400,40),
    "pTB2" :  getLinSpace(0,200,20),
    "dPhiLBmin" : getLinSpace(0.0,3.12,52),
    "dYWH" : getLinSpace(0.0,4.02,67),
    "MET" : getLinSpace(0,300,30),
    "mBBJ" : getLinSpace(0,1000,50),
    "pTJ3" : getLinSpace(0,400,40)
    }

limits = {
    "pTB1" : (0,400,40),
}

def get_binning(h):
    nbins = h.GetNbinsX()
    low_edge_bin = 1
    c_c = 0
    binning_bin = []
    nbins_after_rebin = 10
    for i in range(nbins):
        bin = i + 1
        c_nbins = bin - low_edge_bin + 1  #c_nbins: cumulative nbins
        c_c += h.GetBinContent(bin) # c_c: cumulative content
        if bin == nbins or \
                (c_c > 0.01 and c_nbins > nbins/nbins_after_rebin) or \
                c_c >0.2:
            binning_bin.append(bin)
            low_edge_bin = bin
            c_c = 0
    binning = []
    binning.append(h.GetBinLowEdge(1))
    for bin in binning_bin:
        binning.append(h.GetXaxis().GetBinUpEdge(bin))
    return binning

variables = ["mBB","pTV","Mtop","dRBB","pTB1","pTB2"]#["mBB","pTV","mva","MET","dPhiLBmin","dPhiVBB","dRBB","dYWH","mTW","Mtop","pTB1","pTB2"]
#["mBB","pTV","mva","MET","dPhiLBmin","dPhiVBB","dRBB","dYWH","mTW","Mtop","pTB1","pTB2","mBBJ","pTJ3"]
jets = ["2btag2jet","2btag3jet"]
ptvs = ["75_150ptv","150_250ptv","250ptv"]
heavyFlavours = ["bb","bl","bc","cc","cl","l"]
otherFlavours = [] #["l","cc","cl"] # only used when the stats are low


def main(histogramFilePath):
    sys.argv.append("-b")

    combinations = {}

    # ptv inclusive
    for variable in variables:
        for jet in jets:
            for flavour in heavyFlavours:
                histoName = "W{}_{}_ptv_inclusive_SR_{}".format(flavour,jet,variable)
                combinations[histoName] = []
                for ptv in ptvs:
                    combinations[histoName].append("W{}_{}_{}_SR_{}".format(flavour,jet,ptv,variable))
    
    # other flavours = l+cc+cl
    for variable in variables:
        for jet in jets:
            for ptv in ptvs:
                histoName = "Wother_{}_{}_SR_{}".format(jet,ptv,variable)
                combinations[histoName] = []
                for flavour in otherFlavours:
                    combinations[histoName].append("W{}_{}_{}_SR_{}".format(flavour,jet,ptv,variable))


    # other flavours & inslusive ptv
    for variable in variables:
        for jet in jets:
                histoName = "Wother_{}_ptv_inclusive_SR_{}".format(jet,variable)
                combinations[histoName] = []
                for flavour in otherFlavours:
                    for ptv in ptvs:
                        combinations[histoName].append("W{}_{}_{}_SR_{}".format(flavour,jet,ptv,variable))

    
    # all combinations for HF
    for variable in variables:
        for jet in jets:
            for flavour in heavyFlavours:
                for ptv in ptvs:
                    histogramName = "W{}_{}_{}_SR_{}".format(flavour,jet,ptv,variable)
                    combinations[histogramName] = [histogramName]
    
    closureDict = {}
    
    for name, combination in combinations.items():
        closureDict[name] = plotCombinations(histogramFilePath,name,combination)

    with open("plots/closure.csv","w") as f:
        f.write("name, chi2, KS, KL \n")
        for name, closure in closureDict.items():
            if closure:
                f.write("{}, {}, {}, {} \n".format(name,closure[0],closure[1],closure[2]))
    
    with open("plots/closure_table.tex", "w") as f:
        f.write("\\documentclass{article}\n")
        f.write("\n")
        f.write("\\usepackage[top=0.5in,bottom=0.5in,left=0.5in,right=0.5in]{geometry}\n")
        f.write("\\usepackage{multicol}\n")
        f.write("\\usepackage{xcolor}\n")
        f.write("\n")
        f.write("\\begin{document}\n")
        f.write("\\begin{table}[htbp]\n")
        f.write("\\centering\n")
        f.write("\\begin{tabular}{|ccccccccccccc|}")
        f.write("\hline")
        f.write("\multicolumn{13}{|c|}{$\chi^2$/n.d.f.}")
        f.write("\\\\ \hline")
        f.write("\multicolumn{1}{|c|}{}   & \multicolumn{6}{c|}{2jet}")
        f.write("& \multicolumn{6}{c|}{3jet}")
        f.write("\\\\ \hline")
        f.write("\multicolumn{1}{|c|}{}   & \multicolumn{1}{c|}{$m_{bb}$} & \multicolumn{1}{c|}{$p_T^V$} &")
        f.write("\multicolumn{1}{c|}{$\Delta R_{bb}$} & \multicolumn{1}{c|}{$p_T^{b1}$} &")
        f.write("\multicolumn{1}{c|}{$p_T^{b2}$} & \multicolumn{1}{c|}{$m_{top}$} &")
        f.write("\multicolumn{1}{c|}{$m_{bb}$} & \multicolumn{1}{c|}{$p_T^V$} & \multicolumn{1}{c|}{$\Delta R_{bb}$} & ")
        f.write("\multicolumn{1}{c|}{$p_T^{b1}$} & \multicolumn{1}{c|}{$p_T^{b2}$} & $m_{top}$")
        f.write("\\\\ \hline")

        twoDigits = lambda x: int(x*100)/100.0

        def colorCoding(number):
            if number < 2.0:
                return str(number)
            elif number < 10.0:
                return "{{ \\color{{orange}} {} }}".format(number)
            else:
                return "{{ \\color{{red}} {} }}".format(number)
            
        
        for flavour in ["bb","bc","cc","bl","cl","l"]:
            f.write("\multicolumn{{1}}{{|c|}}{{{0}}} &".format(flavour))
            for jet in ["2jet","3jet"]:
                for variable in ["mBB","pTV","dRBB","pTB1","pTB2","Mtop"]:
                    if jet == "3jet" and variable=="Mtop":
                        f.write("{0}  \\\\".format(colorCoding(twoDigits(closureDict["W{}_2btag{}_ptv_inclusive_SR_{}".format(flavour,jet,variable)][0]))))
                    else:
                        f.write("\multicolumn{{1}}{{c|}}{{{0}}} &".format(colorCoding(twoDigits(closureDict["W{}_2btag{}_ptv_inclusive_SR_{}".format(flavour,jet,variable)][0]))))
            f.write("\hline \n")
        f.write("\\end{tabular}\n\n")
        f.write("\\caption{CARL reweighter closure metrics.}\n")
        f.write("\\label{table:some_label}\n")
        f.write("\\end{table}\n")
        f.write("\\end{document}")

                    
                    

def plotCombinations(histogramFilePath,name,histogramList):
    
    variable = name.split('_')[-1]
    bins = array.array("d",binning[variable])

    ROOT.TH1.SetDefaultSumw2()
    
    histogramNominal = ROOT.TH1F(name,name,len(bins)-1,bins)
    histogramAletrnative = ROOT.TH1F("Mad"+name,"Mad"+name,len(bins)-1,bins)
    histogramVariation = ROOT.TH1F(name+"_SysCarl_W_SHtoMG5__1up",name+"_SysCarl_W_SHtoMG5__1up",len(bins)-1,bins)

    histogramFile = ROOT.TFile(histogramFilePath,"READ")

    for histogram in histogramList:
        h=histogramFile.Get(histogram)
        if h:
            h = h.Rebin(len(bins)-1,name+"_nominal",bins)
            histogramNominal.Add(h)
        else:
            print("Skipping nominal {}".format(histogram))

        h=histogramFile.Get("Mad"+histogram)
        if h:
            h = h.Rebin(len(bins)-1,name+"_alt",bins)
            histogramAletrnative.Add(h)
        else:
            print("Skipping alternative {}".format(histogram))
        
        h=histogramFile.Get("SysCarl_W_SHtoMG5__1up").Get(histogram)
        
        if h:
            h = h.Rebin(len(bins)-1,name+"_var",bins)
            histogramVariation.Add(h)
        else:
            print("Skipping variation {}".format(histogram))

    
    print(name)
    histogramNominal.Print()
    histogramAletrnative.Print()
    histogramVariation.Print()

    
    if histogramNominal.Integral() == 0.0 or histogramAletrnative.Integral() == 0.0 or histogramVariation.Integral() == 0.0:
        print("Some of these histograms are empty. Skipping...")
        return
    
    histogramNominal.Scale(1.0/histogramNominal.Integral())
    histogramAletrnative.Scale(1.0/histogramAletrnative.Integral())
    histogramVariation.Scale(1.0/histogramVariation.Integral())

    """
    if "Wother" in name:
        bins = array.array("d",get_binning(histogramNominal))
        histogramNominal=histogramNominal.Rebin(len(bins)-1,name+"_nominal",bins)
        histogramAletrnative=histogramAletrnative.Rebin(len(bins)-1,name+"_alt",bins)
        histogramVariation=histogramVariation.Rebin(len(bins)-1,name+"_var",bins)
    """
    
    """
    if variable in binning:
        bins = array.array("d",binning[variable])
        #print(bins)
        histogramNominal=histogramNominal.Rebin(len(bins)-1,name+"_nominal",bins)
        histogramAletrnative=histogramAletrnative.Rebin(len(bins)-1,name+"_alt",bins)
        histogramVariation=histogramVariation.Rebin(len(bins)-1,name+"_var",bins)
    """

    units = ""
    if variable in ["mBB","pTV","MET","mTW","Mtop","pTB1","pTB2","mBBJ","pTJ3"]:
        units = " [GeV]"
    
    toLatextDict = {
        "mBB": "m_{bb}",
        "pTV" : "p_{T}^{V}",
        "MET" : "#slash{E}_{T}",
        "mTW" : "m_{T}^{W}",
        "Mtop" : "M_{top}",
        "pTB1" : "p_{T}^{b1}",
        "pTB2" : "p_{T}^{b2}",
        "mBBJ" : "m_{bbj}",
        "pTJ3" : "p_{T}^{j3}",
        "dPhiLBmin" : "#Delta#phi(L,b)_{min}",
        "dPhiVBB" : "#Delta#phi(V,bb)",
        "dRBB" : "#DeltaR_{bb}",
        "dYWH" : "#DeltaY_{WH}",
    }

    latexVariable = variable
    
    if variable in toLatextDict:
        latexVariable = toLatextDict[variable]
    
    
    canvas = ROOT.TCanvas(name, name, 1100, 1200)
    upper = ROOT.TPad("upper", "upper", 0.0, 0.52, 1.0, 1.0)
    ratioPad = ROOT.TPad("ratioPad", "ratioPad", 0.0, 0.29, 1.0, 0.52)
    residualPad = ROOT.TPad("residualPad", "residualPad", 0.0, 0.0, 1.0, 0.29)

    upper.Draw()
    ratioPad.Draw()
    residualPad.Draw()

    upper.SetMargin(0.12,0.03,0.03,0.05)
    ratioPad.SetMargin(0.12,0.03,0.05,0.03)
    residualPad.SetMargin(0.12,0.03,0.25,0.03)
    
    #upper.SetGridx()
    #upper.SetGridy()
    #ratioPad.SetGridx()
    ratioPad.SetGridy()

    upper.cd()

    ROOT.gStyle.SetOptStat(0)

    legend = ROOT.TLegend(0.56, 0.7, 0.96, 0.93)
    legend.SetTextSize(0.04)

    bins=[]
    for i in range(histogramNominal.GetNbinsX()+1):
        bins.append(histogramNominal.GetBinContent(i))
        bins.append(histogramAletrnative.GetBinContent(i))
        bins.append(histogramVariation.GetBinContent(i))

    # return the smallest multiple of 0.05 higher than the largest bin
    y_lim = min(filter(lambda x : x>1.8*max(bins), [5 * float(n) / 100.0 for n in range(38)]))
    
    histogramNominal.GetXaxis().SetLabelSize(0)
    histogramNominal.GetYaxis().SetLabelSize(0.05)
    
    histogramNominal.SetLineColor(ROOT.kBlack)
    histogramNominal.SetMarkerColor(ROOT.kBlack)
    histogramNominal.SetFillStyle(0)
    histogramNominal.SetLineWidth(3)
    #histogramNominal.GetXaxis().SetTitle(variable)
    histogramNominal.GetXaxis().SetTitleSize(0.10)
    
    histogramNominal.GetYaxis().SetTitleSize(0.07)
    histogramNominal.GetYaxis().SetTitleOffset(0.7)
    histogramNominal.GetYaxis().SetTitle("a. u.")
    histogramNominal.SetAxisRange(0.0, y_lim, "Y")

    histogramNominal.SetTitle("")
    

    histogramNominal.Draw("hist")

    histogramAletrnative.SetMarkerColor(2)
    histogramAletrnative.SetLineColor(2)
    histogramAletrnative.SetLineWidth(3)
    histogramAletrnative.Draw("hist same")

    histogramVariation.SetMarkerColor(3)
    histogramVariation.SetLineColor(4)
    histogramVariation.SetLineWidth(3)
    histogramVariation.Draw("hist same")

    
    legend.AddEntry(histogramNominal, "Nominal (Sherpa 2.2.1)", "l")
    legend.AddEntry(histogramAletrnative, "Alternative (MadGraph+Pythia 8)", "l")
    legend.AddEntry(histogramVariation, "CARL * Nominal", "l")
    
    t = ROOT.TLatex()
    t.SetNDC()
    t.SetTextFont(72)
    t.SetTextColor(1)
    t.SetTextSize(0.04)
    t.SetTextAlign(4)

    t.DrawLatex(0.16, 0.90, "ATLAS #it{#bf{Simulation}}")
    t.DrawLatex(0.16, 0.85, "#it{#bf{#sqrt{s} = 13 TeV, 139 fb^{-1}}}")

    if "75_150ptv" in name:
        t.DrawLatex(0.16, 0.80, "#bf{#it{75 GeV} < p_{T} < #it{150 GeV}}")
    elif "150_250ptv" in name:
        t.DrawLatex(0.16, 0.80, "#bf{#it{150 GeV} < p_{T} < #it{250 GeV}}")
    elif "250ptv" in name:
        t.DrawLatex(0.16, 0.80, "#bf{p_{T} > #it{250 GeV}}")
    elif "ptv_inclusive" in name:
        t.DrawLatex(0.16, 0.80, "#bf{p_{T} > #it{75 GeV}}")

    nJ = -1
    if "2jet" in name:
        nJ=2
    elif "3jet" in name:
        nJ=3 
    
    sample = name.split("_")[0]
    
    t.DrawLatex(0.16, 0.75, "#bf{{#it{{{}, 2 b-tags, {} jets}}}}".format(sample,nJ))

    t.SetTextAlign(31)
    t.DrawLatex(0.83, 0.65, "#bf{#it{#chi^{2}/n.d.f.}}")
    t.DrawLatex(0.83, 0.60, "#bf{#it{K-S}}")
    t.DrawLatex(0.83, 0.55, "#bf{#it{K-L}}")

    chi2_test = histogramVariation.Chi2Test(histogramAletrnative,"WW CHI2/NDF")
    KS_test = histogramVariation.KolmogorovTest(histogramAletrnative)
    
    KL_test = 0
    for i in range(histogramNominal.GetNbinsX()):
        KL_test += scipy.special.rel_entr(histogramAletrnative.GetBinContent(i),histogramVariation.GetBinContent(i))
    
    t.SetTextAlign(11)
    t.DrawLatex(0.87, 0.65, "#bf{{#it{{{:.5f}}}}}".format(chi2_test))
    t.DrawLatex(0.87, 0.60, "#bf{{#it{{{:.5f}}}}}".format(KS_test))
    t.DrawLatex(0.87, 0.55, "#bf{{#it{{{:.5f}}}}}".format(abs(KL_test)))
    
    
    baseline = histogramAletrnative.Clone()
    baseline.Divide(histogramAletrnative)
    
    
    baseline.SetFillColorAlpha(ROOT.kRed,0.6)
    baseline.SetFillStyle(3002) # 1002
    baseline.SetLineStyle(1)
    baseline.SetLineColor(0)
    baseline.SetLineWidth(1)
    baseline.SetMarkerStyle(1)
    baseline.SetMarkerSize(0)
    baseline.GetXaxis().SetLabelSize(0)
    baseline.GetXaxis().SetLabelOffset(0.010)
    baseline.GetYaxis().SetLabelSize(0.11)
    baseline.GetYaxis().SetNdivisions(5)
    baseline.GetYaxis().SetTitleOffset(0.45)
    baseline.GetYaxis().SetLabelOffset(0.010)
    baseline.GetYaxis().SetTitle("nominal / alternative")
    baseline.GetYaxis().SetTitleSize(0.11)
    #baseline.GetXaxis().SetTitle(latexVariable+units)
    baseline.GetXaxis().SetTitleSize(0)
    baseline.SetTitle("")
    

    histogramNominalRatio = histogramNominal.Clone()
    histogramNominalRatio.Divide(histogramAletrnative)
    histogramNominalRatio.SetTitle("")
    histogramNominalRatio.SetLineColor(ROOT.kBlack)
    histogramNominalRatio.SetMarkerColor(ROOT.kBlack)
    histogramNominalRatio.SetFillColorAlpha(0, 0)
    
        
    histogramVariationRatio = histogramVariation.Clone()
    histogramVariationRatio.Divide(histogramAletrnative)
    histogramVariationRatio.SetTitle("")
    histogramVariationRatio.SetLineColor(4)
    histogramVariationRatio.SetMarkerColor(3)
    histogramVariationRatio.SetFillColorAlpha(0, 0)

    bins = []
    for i in range(baseline.GetNbinsX()+1):
        bins.append(histogramNominalRatio.GetBinContent(i))
        bins.append(histogramVariationRatio.GetBinContent(i))

    bins = filter(lambda x: x!=0, bins)
    bins = map(lambda x: abs(x-1.0), bins)
    
    limit_options = [0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.9,1.0]

    if max(bins)>max(limit_options):
        y_ratio_max = 1.0
    else:
        y_ratio_max = min(filter(lambda x : x>max(bins), limit_options))
    
    baseline.SetAxisRange(1.0-y_ratio_max, 1.0+y_ratio_max, "Y")
    histogramNominalRatio.SetAxisRange(1.0-y_ratio_max, 1.0+y_ratio_max, "Y")
    histogramVariationRatio.SetAxisRange(1.0-y_ratio_max, 1.0+y_ratio_max, "Y")

    for i in range(baseline.GetNbinsX()+1):
        baseline.SetBinContent(i, 1.0)

    #computing absolute errors for the error band
    for i in range(baseline.GetNbinsX()+1):
        if histogramVariation.GetBinContent(i)!=0 and histogramAletrnative.GetBinContent(i)!=0:
            error = (histogramVariation.GetBinContent(i)/histogramAletrnative.GetBinContent(i)) \
                    *math.sqrt((histogramVariation.GetBinError(i)/histogramVariation.GetBinContent(i))**2 \
                                + (histogramAletrnative.GetBinError(i)/histogramAletrnative.GetBinContent(i))**2)
        else:
            error = 0
        
        baseline.SetBinError(i, error)
    

    legend.AddEntry(baseline, "Relative statistical error", "f")
    legend.Draw()
    
    ratioPad.cd()
        
    baseline.Draw("L E2")
    histogramNominalRatio.Draw("same hist")
    histogramVariationRatio.Draw("same hist")

    residualPad.cd()
    
    histogramNominalResidual = histogramNominalRatio.Clone()
    histogramVariationResidual= histogramVariationRatio.Clone()
    oneSigmaBand = baseline.Clone()
    threeSigmaBand = baseline.Clone()
    fiveSigmaBand = baseline.Clone()

    for n in range(baseline.GetNbinsX()+1):
        if histogramNominal.GetBinError(n) != 0 and histogramAletrnative.GetBinError(n) != 0:
            histogramNominalResidual.SetBinContent(n,(histogramNominal.GetBinContent(n)-histogramAletrnative.GetBinContent(n))/math.sqrt(histogramNominal.GetBinError(n)**2 + histogramAletrnative.GetBinError(n)**2))
        else:
            histogramNominalResidual.SetBinContent(n,1.0)
        if histogramVariation.GetBinError(n) != 0 and histogramAletrnative.GetBinError(n) != 0:
            histogramVariationResidual.SetBinContent(n,(histogramVariation.GetBinContent(n)-histogramAletrnative.GetBinContent(n))/math.sqrt(histogramVariation.GetBinError(n)**2 + histogramAletrnative.GetBinError(n)**2))
        else:
            histogramVariationResidual.SetBinContent(n,1.0)
        
        oneSigmaBand.SetBinError(n,1.0)
        threeSigmaBand.SetBinError(n,3.0)
        fiveSigmaBand.SetBinError(n,5.0)
        oneSigmaBand.SetBinContent(n,0.0)
        threeSigmaBand.SetBinContent(n,0.0)
        fiveSigmaBand.SetBinContent(n,0.0)

    
    fiveSigmaBand.GetXaxis().SetLabelSize(0.10)
    fiveSigmaBand.GetXaxis().SetLabelOffset(0.010)
    fiveSigmaBand.GetYaxis().SetLabelSize(0.09)
    fiveSigmaBand.GetYaxis().SetNdivisions(8)
    fiveSigmaBand.GetYaxis().SetTitleOffset(0.5)
    fiveSigmaBand.GetYaxis().SetLabelOffset(0.010)
    fiveSigmaBand.GetYaxis().SetTitle("residual")
    fiveSigmaBand.GetYaxis().SetTitleSize(0.10)
    fiveSigmaBand.GetXaxis().SetTitle(latexVariable+units)
    fiveSigmaBand.GetXaxis().SetTitleSize(0.10)
    fiveSigmaBand.SetTitle("")

    oneSigmaBand.SetLineWidth(0)
    oneSigmaBand.SetLineColor(ROOT.kGreen-9)
    oneSigmaBand.SetLineStyle(1)
    oneSigmaBand.SetFillColorAlpha(ROOT.kGreen-9,1.0)
    oneSigmaBand.SetFillStyle(1001)
    threeSigmaBand.SetLineWidth(0)
    threeSigmaBand.SetLineStyle(1)
    threeSigmaBand.SetLineColor(ROOT.kGreen-9)
    threeSigmaBand.SetFillColorAlpha(ROOT.kYellow-9,1.0)
    threeSigmaBand.SetFillStyle(1001)
    fiveSigmaBand.SetLineWidth(0)
    fiveSigmaBand.SetLineStyle(1)
    fiveSigmaBand.SetLineColor(ROOT.kGreen-9)
    fiveSigmaBand.SetFillColorAlpha(ROOT.kRed-9,1.0)
    fiveSigmaBand.SetFillStyle(1001)

    
    y_residue_lim = 8.5
    histogramNominalResidual.SetAxisRange(-y_residue_lim, y_residue_lim, "Y")
    histogramVariationResidual.SetAxisRange(-y_residue_lim, y_residue_lim, "Y")
    oneSigmaBand.SetAxisRange(-y_residue_lim, y_residue_lim, "Y")
    threeSigmaBand.SetAxisRange(-y_residue_lim, y_residue_lim, "Y")
    fiveSigmaBand.SetAxisRange(-y_residue_lim, y_residue_lim, "Y")

    
    fiveSigmaBand.Draw("E2")
    threeSigmaBand.Draw("E2 same")
    oneSigmaBand.Draw("E2 same")
    histogramNominalResidual.Draw("hist same")
    histogramVariationResidual.Draw("same hist")

    residualLegend = ROOT.TLegend(0.65, 0.86, 0.96, 0.95)
    residualLegend.SetTextSize(0.04)
    residualLegend.SetNColumns(3)
    residualLegend.SetTextSize(0.07)
    
    residualLegend.AddEntry(oneSigmaBand, "1 #sigma", "f")
    residualLegend.AddEntry(threeSigmaBand, "3 #sigma", "f")
    residualLegend.AddEntry(fiveSigmaBand, "5 #sigma", "f")
    
    residualLegend.Draw()
    
    canvas.Update()
    canvas.Draw()

    canvas.SaveAs("plots/{}.pdf".format(name))
    canvas.SaveAs("plots/{}.png".format(name))

    return chi2_test, KS_test, KL_test
    
if __name__ == "__main__":
    main(sys.argv[1])
