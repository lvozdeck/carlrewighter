
import ROOT
import sys

regions = ["CRLow","SR","CRHigh"]
jets = ["2btag2jet","2btag3jet"]
ptvs = ["75_150ptv","150_250ptv","250ptv"]
flavours = ["bb","bl","bc","l","cc","cl"]

path = sys.argv[1]

histogramFile = ROOT.TFile(path,"READ")

N = 0.0

for jet in jets:
    for flavour in flavours:
        for ptv in ptvs:
            for region in regions:
                print("W{}_{}_{}_{}_mva".format(flavour,jet,ptv,region))
                h = histogramFile.Get("W{}_{}_{}_{}_mva".format(flavour,jet,ptv,region))
                if h:
                    N += h.GetEntries()

print(N)
